package com.morais.reginaldo.health.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "health-check")
public class HealthConfigurationProperties {

    private boolean showDetails;
}
