package com.morais.reginaldo.configuration;

import com.morais.reginaldo.configuration.properties.RestTemplateConfigurationProperties;
import lombok.RequiredArgsConstructor;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
@RequiredArgsConstructor
@EnableConfigurationProperties(RestTemplateConfigurationProperties.class)
public class RestTemplateConfiguration {

    private final RestTemplateConfigurationProperties restTemplateConfigurationProperties;

    @Bean("restTemplate")
    public RestTemplate restTemplate() {

        final RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
        return restTemplate;
    }

    private ClientHttpRequestFactory getClientHttpRequestFactory() {

        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(restTemplateConfigurationProperties.getConnectTimeout())
                .setConnectionRequestTimeout(restTemplateConfigurationProperties.getConnectionRequestTimeout())
                .setSocketTimeout(restTemplateConfigurationProperties.getSocketTimeout())
                .build();

        CloseableHttpClient client = HttpClientBuilder
                .create()
                .setDefaultRequestConfig(config)
                .build();

        return new HttpComponentsClientHttpRequestFactory(client);
    }
}
