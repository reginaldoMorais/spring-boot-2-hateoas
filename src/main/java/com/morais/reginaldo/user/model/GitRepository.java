package com.morais.reginaldo.user.model;

import com.morais.reginaldo.user.enumeration.Git;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GitRepository {

    private Git git;

    private String url;

    private String name;

    private String description;

    private boolean visible;
}
