package com.morais.reginaldo.health.check;

import org.bson.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Status;
import org.springframework.boot.actuate.mongo.MongoHealthIndicator;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class MongoTemplateHealthIndicatorTest {

    @Mock
    private MongoTemplate mongoTemplate;

    @Mock
    MongoHealthIndicator mongoHealthIndicator;

    private MongoTemplateHealthIndicator mongoTemplateHealthIndicator;

    @BeforeEach
    public void before() throws Exception {

        MockitoAnnotations.initMocks(this);
        mongoTemplateHealthIndicator = new MongoTemplateHealthIndicator(mongoTemplate);
    }

    @Test
    public void test_check_health_of_mongo_template_should_return_health_details() throws Exception {

        Health health = new Health.Builder().status(Status.UP).build();

        when(mongoTemplate.executeCommand(anyString())).thenReturn(new Document("version", "3.2"));
        when(mongoHealthIndicator.getHealth(ArgumentMatchers.anyBoolean())).thenReturn(health);

        Health healthResult = mongoTemplateHealthIndicator.health();

        assertEquals(Status.UP, healthResult.getStatus());
        assertTrue(healthResult.getDetails().containsKey("version"));
    }
}