package com.morais.reginaldo.gitRepositories.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "git-repositories")
public class GitRepositoriesConfigurationProperties {

    private String url;
}
