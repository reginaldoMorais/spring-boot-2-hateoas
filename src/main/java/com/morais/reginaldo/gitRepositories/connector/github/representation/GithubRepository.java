package com.morais.reginaldo.gitRepositories.connector.github.representation;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GithubRepository {

    private long id;

    @JsonProperty("html_url")
    private String url;

    private String name;

    private String description;

    @JsonProperty("private")
    private boolean visible;
}
