package com.morais.reginaldo.user.representation;

import com.morais.reginaldo.user.enumeration.UserStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserPatchRequestTest {

    private UserPatchRequest userPatchRequest;

    @BeforeEach
    public void before() throws Exception {
        userPatchRequest = new UserPatchRequest();
    }

    @Test
    public void test_instance_new_user_patch_request_without_arguments_should_return_false() {

        assertFalse(userPatchRequest.hasSomeAttribute());
    }

    @Test
    public void test_instance_new_user_patch_request_with_name_should_return_true() {

        userPatchRequest.setName("John Doe");
        assertTrue(userPatchRequest.hasSomeAttribute());
    }

    @Test
    public void test_instance_new_user_patch_request_with_email_should_return_true() {

        userPatchRequest.setEmail("john.doe@exemple.com");
        assertTrue(userPatchRequest.hasSomeAttribute());
    }

    @Test
    public void test_instance_new_user_patch_request_with_git_username_should_return_true() {

        userPatchRequest.setGitUsername("john.doe");
        assertTrue(userPatchRequest.hasSomeAttribute());
    }

    @Test
    public void test_instance_new_user_patch_request_with_status_should_return_true() {

        userPatchRequest.setStatus(UserStatus.ACTIVE);
        assertTrue(userPatchRequest.hasSomeAttribute());
    }
}
