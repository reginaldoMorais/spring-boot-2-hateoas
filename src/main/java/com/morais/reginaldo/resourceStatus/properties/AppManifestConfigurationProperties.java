package com.morais.reginaldo.resourceStatus.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "app-manifest")
public class AppManifestConfigurationProperties {

    private String createdBy;

    private String buildJDK;

    private String applicationName;

    private String implementationBuild;

    private String implementationVersion;
}
