package com.morais.reginaldo.gitRepositories;

import com.morais.reginaldo.gitRepositories.connector.GitConnector;
import com.morais.reginaldo.gitRepositories.connector.github.representation.GithubResponse;
import com.morais.reginaldo.user.enumeration.Git;
import com.morais.reginaldo.user.model.GitRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@AllArgsConstructor
@Service
public class GitServiceImpl implements GitService {

    private GitFactory gitFactory;

    @Override
    public List<GitRepository> getAll(final String gitUsername) {

        List<GitRepository> gitRepositoriesFull = new ArrayList<>();
        List<GitRepository> gitHubRepositories = getByGithub(gitUsername);

        if (gitHubRepositories.size() > 0) {
            gitRepositoriesFull.addAll(gitHubRepositories);
        }

        return gitRepositoriesFull;
    }

    @Override
    public List<GitRepository> getByGithub(final String gitUsername) {

        GitConnector connector = gitFactory.createConnector(Git.GITHUB);
        GithubResponse githubResponse = (GithubResponse) connector.get(gitUsername);

        return githubResponse.getGithubRepositories().stream()
                .map(githubRepository -> GitRepository.builder()
                        .git(Git.GITHUB)
                        .url(githubRepository.getUrl())
                        .name(githubRepository.getName())
                        .description(githubRepository.getDescription())
                        .visible(githubRepository.isVisible())
                        .build())
                .collect(Collectors.toList());
    }
}
