package com.morais.reginaldo.user.service;

import com.morais.reginaldo.user.exception.UserNotFoundException;
import com.morais.reginaldo.user.representation.UserRequest;
import com.morais.reginaldo.user.representation.UserResponse;
import com.morais.reginaldo.user.representation.UserPatchRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

public interface UserService {

    UserResponse create(UserRequest userRequest);

    @Transactional
    UserResponse update(UUID id, UserRequest userRequest) throws UserNotFoundException;

    @Transactional
    void patch(UUID id, UserPatchRequest userPatchRequest) throws UserNotFoundException;

    @Transactional
    void delete(UUID id) throws UserNotFoundException;

    UserResponse get(UUID id) throws UserNotFoundException;

    Page<UserResponse> list(Pageable pageable) throws UserNotFoundException;
}
