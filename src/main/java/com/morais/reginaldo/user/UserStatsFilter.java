package com.morais.reginaldo.user;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.UUID;

@Slf4j
@Component
@WebFilter("/users")
public class UserStatsFilter implements Filter {

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException { }

    @Autowired
    Environment environment;

    @Override
    public void doFilter(final ServletRequest req, final ServletResponse resp, final FilterChain chain)
            throws IOException, ServletException {

        Instant now = Instant.now();

        try {
            MDC.put("tid", UUID.randomUUID().toString());
            chain.doFilter(req, resp);
        } finally {
            Duration duration = Duration.between(now, Instant.now());
            log.info("{} {} request time: [{} ms]",
                    ((HttpServletRequest) req).getMethod(),
                    ((HttpServletRequest) req).getRequestURI(),
                    duration.toMillis());

            MDC.clear();
        }
    }

    @Override
    public void destroy() { }
}
