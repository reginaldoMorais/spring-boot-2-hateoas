package com.morais.reginaldo.health.service;

import com.morais.reginaldo.health.check.MongoTemplateHealthIndicator;
import com.morais.reginaldo.health.check.RestTemplateHealthIndicator;
import com.morais.reginaldo.health.properties.HealthConfigurationProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Status;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class HealthServiceImplTest {

    @Mock
    private MongoTemplateHealthIndicator mongoTemplateHealthIndicator;

    @Mock
    private RestTemplateHealthIndicator restTemplateHealthIndicator;

    @Mock
    private HealthConfigurationProperties configurationProperties;

    private HealthService healthService;

    @BeforeEach
    public void before() throws Exception {

        MockitoAnnotations.initMocks(this);
        healthService = new HealthServiceImpl(mongoTemplateHealthIndicator, restTemplateHealthIndicator, configurationProperties);
    }

    @Test
    public void test_get_health_should_return_health_without_details() throws Exception {

        when(mongoTemplateHealthIndicator.health()).thenReturn(new Health.Builder().status(Status.UP).build());
        when(restTemplateHealthIndicator.health()).thenReturn(new Health.Builder().status(Status.UP).build());
        when(configurationProperties.isShowDetails()).thenReturn(false);

        Health healthResult = healthService.getHealth();

        assertEquals(Status.UP, healthResult.getStatus());
        assertTrue(healthResult.getDetails().isEmpty());
    }

    @Test
    public void test_get_health_with_some_dependency_down_should_return_health_down_without_details() throws Exception {

        when(mongoTemplateHealthIndicator.health()).thenReturn(new Health.Builder().status(Status.DOWN).build());
        when(restTemplateHealthIndicator.health()).thenReturn(new Health.Builder().status(Status.UP).build());
        when(configurationProperties.isShowDetails()).thenReturn(false);

        Health healthResult = healthService.getHealth();

        assertEquals(Status.DOWN, healthResult.getStatus());
        assertTrue(healthResult.getDetails().isEmpty());
    }

    @Test
    public void test_get_health_details_should_return_health_with_details() throws Exception {

        when(mongoTemplateHealthIndicator.health()).thenReturn(new Health.Builder().status(Status.UP).build());
        when(restTemplateHealthIndicator.health()).thenReturn(new Health.Builder().status(Status.UP).build());
        when(configurationProperties.isShowDetails()).thenReturn(true);

        Health healthResult = healthService.getHealth();

        assertEquals(Status.UP, healthResult.getStatus());
        assertTrue(healthResult.getDetails().containsKey("mongodb"));
    }

    @Test
    public void test_get_health_with_some_dependency_down_should_return_health_down() throws Exception {

        when(mongoTemplateHealthIndicator.health()).thenReturn(new Health.Builder().status(Status.UP).build());
        when(restTemplateHealthIndicator.health()).thenReturn(new Health.Builder().status(Status.DOWN).build());
        when(configurationProperties.isShowDetails()).thenReturn(true);

        Health healthResult = healthService.getHealth();

        assertEquals(Status.DOWN, healthResult.getStatus());
        assertTrue(healthResult.getDetails().containsKey("git-rest-template"));

        Health healthGitRestTemplate = (Health) healthResult.getDetails().get("git-rest-template");
        assertEquals(Status.DOWN, healthGitRestTemplate.getStatus());
    }
}
