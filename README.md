# Spring Boot 2: Rest API Hateoas

Teste de implementação de Hateoas.

[Documentação](http://localhost:8080/doc.html)

## Sumário

- [Spring Boot 2: Rest API Hateoas](#spring-boot-2-rest-api-hateoas)
  - [Sumário](#sum%c3%a1rio)
  - [Pré requisitos](#pr%c3%a9-requisitos)
    - [Instalando no Ubuntu Linux](#instalando-no-ubuntu-linux)
      - [Instalando o JDK](#instalando-o-jdk)
        - [Via SDKMAN](#via-sdkman)
        - [Via Oracle JDK](#via-oracle-jdk)
      - [Instalando o Docker](#instalando-o-docker)
      - [Baixando a imagem docker do MongoDB](#baixando-a-imagem-docker-do-mongodb)
      - [Criando o container do MongoDB](#criando-o-container-do-mongodb)
    - [Configurando IDE](#configurando-ide)
      - [Checkstyle 8.23](#checkstyle-823)
        - [Checkstyle: Jetbrains Intellij IDEA](#checkstyle-jetbrains-intellij-idea)
          - [Instalação Checkstyle](#instala%c3%a7%c3%a3o-checkstyle)
          - [Configuração Checkstyle](#configura%c3%a7%c3%a3o-checkstyle)
      - [Spotbugs (Findbugs)](#spotbugs-findbugs)
        - [Spotbugs: Jetbrains Intellij IDEA](#spotbugs-jetbrains-intellij-idea)
          - [Instalação Spotbugs](#instala%c3%a7%c3%a3o-spotbugs)
          - [Configuração Spotbugs](#configura%c3%a7%c3%a3o-spotbugs)
  - [Baixando o projeto](#baixando-o-projeto)
    - [Git Commit](#git-commit)
      - [Mensagem de commit](#mensagem-de-commit)
        - [Exemplo 1](#exemplo-1)
        - [Exemplo 2](#exemplo-2)
  - [Executando a Aplicação](#executando-a-aplica%c3%a7%c3%a3o)
  - [Team](#team)

## Pré requisitos

Recomendamos que você possua os seguintes requisitos, antes de executar o projeto:

1. JDK 8
2. Docker
3. Imagem Docker do MongoDB 3.2

### Instalando no Ubuntu Linux

#### Instalando o JDK

##### Via SDKMAN

O SDKMAN é um gerenciador de versões para Java, Gradle, Groovy, etc. Para baixa-lo execute:

```bash
curl -s "https://get.sdkman.io" | bash
```

Para listar as opções de JDK:

```bash
sdk list java
```

Após instalação instale o JDK de sua preferência:

```bash
sdk install java 13.0.1-zulu
```

Referência: [https://sdkman.io/](https://sdkman.io/).

##### Via Oracle JDK

```bash
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer
```

#### Instalando o Docker

```bash
sudo apt-get install \
 apt-transport-https \ ca-certificates \ curl \ software-properties-common
```

``` bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

``` bash
sudo apt-key fingerprint 0EBFCD88
```

``` bash
sudo add-apt-repository \
 "deb [arch=amd64] https://download.docker.com/linux/ubuntu \ $(lsb_release -cs) \ stable"
```

``` bash
sudo apt-get update
```

``` bash
sudo apt-get install docker-ce
```

Referência: [Docker install](https://docs.docker.com/install/)

#### Baixando a imagem docker do MongoDB

``` bash
docker pull mongo
```

#### Criando o container do MongoDB

``` bash
docker run --name mongo -p 27017:27017 --name seu-nome-para-o-mongo -d mongo
```

A partir desse momento, você terá na sua máquina tudo necessário para executar o projeto da API Multi Adquirente.

### Configurando IDE

Para melhor aproveitamento na fase de desenvolvimento separamos este tópico para ajudar a configurar o Checkstyle e Findbugs em sua IDE. Ambos ajudarão a mander o código fonte com as conformidades exigidas.

#### Checkstyle 8.23

##### Checkstyle: Jetbrains Intellij IDEA

###### Instalação Checkstyle

1. No menu suspenso da IDE selecione **File > Settings**
2. Será exibido a janela suspensa de configurações. Em seguida selecione a opção **Plugins**.
3. Na tela de instalação de plugins clique no botão **Browser Repositories**.
4. Busque por **Checkstyle** e ao final da instalação reinicie a IDE.

###### Configuração Checkstyle

1. No menu suspenso da IDE selecione **File > Settings**
2. Será exibido a janela suspensa de configurações. Em seguida selecione a opção **Other settings > Checkstyle**.
3. Na área de **Configuration File** clique no botão **+**.
4. Selecione o xml de configuração na pasta **/config/checkstyle/checkstyle.xml**.
5. Marque a configuração que acabou de importar e aplique as modificações.

#### Spotbugs (Findbugs)

##### Spotbugs: Jetbrains Intellij IDEA

###### Instalação Spotbugs

1. No menu suspenso da IDE selecione **File > Settings**
2. Será exibido a janela suspensa de configurações. Em seguida selecione a opção **Plugins**.
3. Na tela de instalação de plugins clique no botão **Browser Repositories**.
4. Busque por **Findbugs** e ao final da instalação reinicie a IDE.

###### Configuração Spotbugs

1. No menu suspenso da IDE selecione **File > Settings**
2. Será exibido a janela suspensa de configurações. Em seguida selecione a opção **Other settings > Findbugs-IDEA**.
3. Clique na aba **Filter**.
4. Na área de **Exclude filter Files** clique no botão **+**.
5. Selecione o xml de configuração na pasta **/config/spotbugs/excludeFilter.xml**.
6. Marque a configuração que acabou de importar e aplique as modificações.

## Baixando o projeto

Para clonar o projeto siga os comandos:

``` bash
git clone git@gitlab.com:reginaldoMorais/spring-boot-2-hateoas.git
cd spring-boot-2-hateoas
```

### Git Commit

O primeiro passo para iniciar um desenvolvimento é verificar com o time a partir de qual branch deve-se criar um novo branch de desenvolvimento.
Supondo que iremos iniciar o desenvolvimento a partir do branch release/sprint-1:

``` bash
git checkout -b feature/{descrição do branch}
```

#### Mensagem de commit

A messagem de commit deve seguir a convensão proposta pela equipe:

``` note
tipo: assunto
```

ou

``` note
tipo(escopo): assunto
```

Tipos:

- feat: Nova funcionalidade
- fix: Correções
- docs: Mudanças de Documentação
- style: Formatação de código
- refactor: Refatoração de código
- test: Adição ou refatoração de testes
- chore: Atualização de tarefas

Referencias:
[Git Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)
[Sparkbox](https://seesparkbox.com/foundry/semantic_commit_messages)
[Karma](http://karma-runner.github.io/1.0/dev/git-commit-msg.html)

##### Exemplo 1

``` bash
git commit -m "feat(autorizacao): recuperando autorizacao por id"
```

##### Exemplo 2

Se preferir, inclua a referência da história/task no commit, ou qualquer Smart Commit command.

Referência: [Confluence Atlassian - Using smart commits](https://confluence.atlassian.com/fisheye/using-smart-commits-298976812.html).

``` bash
git commit -m "feat(autorizacao): TASK-NUMBER #comment recuperando autorizacao por id #time 1h #resolve"
```

## Executando a Aplicação

O comando abaixo executará a task default do Gradle para gerar as classes dinâmicas, gerar os stubs e efetuar o download das dependências do projeto java:

``` bash
./gradlew
```

O comando abaixo executará o projeto java na porta 8080 da máquina local:

``` bash
./gradlew bootRun
```

O comando abaixo executará o projeto java apontando para o ambiente de Development:

``` bash
./gradlew bootRun -Dspring.profiles.active=development
```

O comando abaixo verifica se o projeto está sendo executado:

``` bash
$ curl http://localhost:8080/health -v
* Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /health HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.47.0
> Accept: */*
>
< HTTP/1.1 200 OK
< Date: Fri, 09 Jun 2017 15:06:14 GMT
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< X-Frame-Options: DENY
< Content-Type: application/json;charset=utf-8
< Connection: close
<
* Closing connection 0
{"status":"OK","message":"The application is fully functional.","dependencies":[]}
```

Se a resposta for semelhante à acima, então, a aplicação está funcional no ambiente local.

## Team

[Reginaldo Morais](reginaldo.cmorais@gmail.com)