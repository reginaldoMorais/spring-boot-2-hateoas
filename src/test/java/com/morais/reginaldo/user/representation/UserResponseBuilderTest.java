package com.morais.reginaldo.user.representation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserResponseBuilderTest {

    private UserResponseBuilder userResponseBuilder;

    @BeforeEach
    public void before() throws Exception {

        MockitoAnnotations.initMocks(this);
        userResponseBuilder = new UserResponseBuilder();
    }

    @Test
    public void test_instance_new_user_response_builder_without_arguments_should_return_success() {

        UserResponseBuilder userResponseBuilder = UserResponseBuilder.anUserResponse();
        assertEquals(UserResponseBuilder.class, userResponseBuilder.getClass());
    }

    @Test
    public void test_build_new_user_by_user_response_builder_without_link_should_return_success() {

        UserResponseBuilder userResponseBuilder = UserResponseBuilder.anUserResponse();
        UserResponse userResponse = UserResponseBuilder.anUserResponse().build();
        assertTrue(userResponse.getLinks().hasSize(0));
    }
}
