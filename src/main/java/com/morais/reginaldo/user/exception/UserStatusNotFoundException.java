package com.morais.reginaldo.user.exception;

public class UserStatusNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public UserStatusNotFoundException() {
        super("User status not found");
    }

    public UserStatusNotFoundException(final String message) {
        super(message);
    }
}
