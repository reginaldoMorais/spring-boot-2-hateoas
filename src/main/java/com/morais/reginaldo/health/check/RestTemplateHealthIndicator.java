package com.morais.reginaldo.health.check;

import lombok.AllArgsConstructor;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Collections;

@AllArgsConstructor
@Component
public class RestTemplateHealthIndicator implements HealthIndicator {

    private RestTemplate restTemplate;

    @Override
    public Health health() {

        Health.Builder builder = new Health.Builder();

        try {
            final String url = "https://api.github.com/";
            final HttpEntity requestEntity = new HttpEntity(getHeader());

            final ResponseEntity<String> response = restTemplate.exchange(
                    RequestEntity.get(new URI(url)).build(),
                    String.class);

            HttpStatus status = response.getStatusCode();

            if (status.is2xxSuccessful()) {
                return builder.status(Status.UP).build();
            }

            return builder.status(Status.DOWN).build();
        } catch (Exception e) {
            return builder.status(Status.DOWN).build();
        }
    }

    private HttpHeaders getHeader() {

        final HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        header.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        return header;
    }
}
