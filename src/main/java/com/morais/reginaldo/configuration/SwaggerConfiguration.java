package com.morais.reginaldo.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableSwagger2WebMvc
public class SwaggerConfiguration {

    @Bean
    public Docket greetingApi() {

        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.morais.reginaldo"))
                .paths(PathSelectors.regex("/user.*"))
                .build()
                .apiInfo(this.getMetaData())
                .protocols(this.getProtocols());
    }

    private ApiInfo getMetaData() {

        return new ApiInfoBuilder()
                .title("User Rest API")
                .description("# Introduction"
                        + "\nREST API que expoe os recursos de Usuários"
                        + "\n\n# Teste de implementação de Hateoas")
                .version("1.0.0")
                .contact(
                        new Contact(
                                "Reginaldo Morais",
                                null,
                                "reginaldo.cmorais@gmail.com")
                )
                .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
                .build();
    }

    private Set<String> getProtocols() {

        Set<String> protocols = new HashSet<>();
        protocols.add("http");
        protocols.add("https");
        return protocols;
    }
}
