package com.morais.reginaldo.user.representation;

import com.morais.reginaldo.user.enumeration.UserStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@ApiModel(description = "Payload of patch Status User")
public class UserPatchRequest {

    @Size(min = 2)
    @ApiModelProperty(value = "Name of User. Min of 2", required = true, example = "John Doe")
    private String name;

    @Email
    @ApiModelProperty(value = "E-mail of User", required = true, example = "john.doe@example.com")
    private String email;

    @ApiModelProperty(value = "Git username", required = true, example = "john.doe")
    private String gitUsername;

    @ApiModelProperty(
            value = "Status active of User",
            allowableValues = "ACTIVE, INACTIVE",
            required = true,
            example = "ACTIVE")
    private UserStatus status;

    public boolean hasSomeAttribute() {
        return !(this.name == null && this.email == null && this.gitUsername == null && this.status == null);
    }
}
