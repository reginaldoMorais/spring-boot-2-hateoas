package com.morais.reginaldo.resourceStatus;

import com.morais.reginaldo.resourceStatus.representation.ResourceStatusResponse;
import com.morais.reginaldo.resourceStatus.service.ResourceStatusService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ResourceStatusController.class)
@AutoConfigureMockMvc
@EnableSpringDataWebSupport
public class ResourceStatusControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ResourceStatusService resourceStatusService;

    @BeforeEach
    public void Before() {

        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .build();
    }

    @Test
    public void test_get_resource_status_should_return_success() throws Exception {

        ResourceStatusResponse resourceStatusResponse = new ResourceStatusResponse(
                "Gradle Version 6.2.2",
                "13.0.2 (Azul Systems, Inc. 13.0.2+6-MTS)",
                "spring-hateoas",
                "20200402",
                "2.1");

        when(resourceStatusService.get()).thenReturn(resourceStatusResponse);

        final ResultActions resultActions = mockMvc.perform(get("/resource-status"));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.applicationName").value("spring-hateoas"));
    }
}
