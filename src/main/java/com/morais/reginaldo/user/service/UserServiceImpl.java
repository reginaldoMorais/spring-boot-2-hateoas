package com.morais.reginaldo.user.service;

import com.morais.reginaldo.gitRepositories.GitService;
import com.morais.reginaldo.user.exception.UserNotFoundException;
import com.morais.reginaldo.user.model.User;
import com.morais.reginaldo.user.repository.UserRepository;
import com.morais.reginaldo.user.representation.UserRequest;
import com.morais.reginaldo.user.representation.UserResponse;
import com.morais.reginaldo.user.representation.UserResponseBuilder;
import com.morais.reginaldo.user.representation.UserPatchRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@AllArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    private GitService gitService;

    /**
     * Create a new user.
     *
     * @param userRequest
     * @return userResponse
     */
    @Override
    public UserResponse create(final UserRequest userRequest) {

        log.info("UserService.create({})", userRequest);

        return this.insert(UUID.randomUUID(), userRequest);
    }

    /**
     * Update a user by id.
     *
     * @param id
     * @param userRequest
     * @return userResponse
     * @throws UserNotFoundException
     */
    @Transactional
    @Override
    public UserResponse update(final UUID id, final UserRequest userRequest) {

        log.info("UserService.update({})", userRequest);

        Optional<User> userOptional = userRepository.findById(id);

        if (userOptional.isPresent()) {
            User user = userOptional.get();
            user.setName(userRequest.getName());
            user.setEmail(userRequest.getEmail());
            user.setGitRepositories(gitService.getAll(userRequest.getGitUsername()));
            user.setStatus(userRequest.getStatus());
            user.setUpdatedAt(Instant.now());

            return UserResponseBuilder
                    .anUserResponse(userRepository.save(user))
                    .withNewUser(false)
                    .build();
        }

        return this.insert(id, userRequest);
    }

    /**
     * Update some attributes of a user by id.
     *
     * @param id
     * @param userPatchRequest
     * @return userResponse
     * @throws UserNotFoundException
     */
    @Transactional
    @Override
    public void patch(final UUID id, final UserPatchRequest userPatchRequest) {

        log.info("UserService.patch({}, {})", id, userPatchRequest);

        User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        user.setUpdatedAt(Instant.now());

        if (userPatchRequest.getName() != null) {
            user.setName(userPatchRequest.getName());
        }

        if (userPatchRequest.getEmail() != null) {
            user.setEmail(userPatchRequest.getEmail());
        }

        if (userPatchRequest.getGitUsername() != null) {
            user.setGitRepositories(gitService.getAll(userPatchRequest.getGitUsername()));
        }

        if (userPatchRequest.getStatus() != null) {
            user.setStatus(userPatchRequest.getStatus());
        }

        userRepository.save(user);
    }

    /**
     * Delete a user by id.
     *
     * @param id
     * @throws UserNotFoundException
     */
    @Transactional
    @Override
    public void delete(final UUID id) {

        log.info("UserService.delete({})", id.toString());
        User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        userRepository.delete(user);
    }

    /**
     * Get a user by id.
     *
     * @param id
     * @return userResponse
     * @throws UserNotFoundException
     */
    @Override
    public UserResponse get(final UUID id) {

        log.info("UserService.get({})", id.toString());
        User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        return UserResponseBuilder.anUserResponse(user).build();
    }

    /**
     * Get all users.
     *
     * @param pageable
     * @return userResponsePage
     * @throws UserNotFoundException
     */
    @Override
    public Page<UserResponse> list(final Pageable pageable) {

        log.info("UserService.list({})", pageable);
        Page<User> userPage = Optional.ofNullable(userRepository.findAll(pageable))
                .orElseThrow(UserNotFoundException::new);
        return userPage.map(u -> UserResponseBuilder.anUserResponse(u).build());
    }

    /**
     * Generic method to save a new user.
     *
     * @param id
     * @param userRequest
     * @return userResponse
     */
    private UserResponse insert(final UUID id, final UserRequest userRequest) {

        log.info("UserService.insert({}, {})", id, userRequest);

        User user = new User(
                id,
                userRequest.getName(),
                userRequest.getEmail(),
                userRequest.getStatus(),
                gitService.getAll(userRequest.getGitUsername()),
                Instant.now(),
                Instant.now());

        log.info("User {}", user);

        return UserResponseBuilder
                .anUserResponse(userRepository.insert(user))
                .withNewUser(true)
                .build();
    }
}
