package com.morais.reginaldo.user.enumeration;

import com.morais.reginaldo.user.exception.UserStatusNotFoundException;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UserStatusTest {

    @Test
    public void test_get_user_status_codes_should_return_success() throws Exception {

        List<Integer> codes = UserStatus.getCodes();
        assertEquals(0, codes.get(0));
        assertEquals(1, codes.get(1));
    }

    @Test
    public void test_find_user_status_by_name_should_return_success() throws Exception {

        UserStatus userStatus = UserStatus.findByName("ACTIVE");
        assertEquals("ACTIVE", userStatus.name());
    }

    @Test
    public void test_find_user_status_by_wrong_name_should_return_exception() throws Exception {

        assertThrows(UserStatusNotFoundException.class, () -> {
            UserStatus.findByName("STATUS");
        });
    }

    @Test
    public void test_find_user_status_by_code_should_return_success() throws Exception {

        UserStatus userStatus = UserStatus.findByCode(0);
        assertEquals("INACTIVE", userStatus.name());
    }

    @Test
    public void test_find_user_status_by_wrong_code_should_return_exception() throws Exception {

        assertThrows(UserStatusNotFoundException.class, () -> {
            UserStatus.findByCode(2);
        });
    }
}
