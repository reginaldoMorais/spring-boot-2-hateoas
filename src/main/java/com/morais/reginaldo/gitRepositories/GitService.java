package com.morais.reginaldo.gitRepositories;

import com.morais.reginaldo.user.model.GitRepository;

import java.util.List;

public interface GitService {

    List<GitRepository> getAll(String gitUsername);

    List<GitRepository> getByGithub(String gitUsername);
}
