package com.morais.reginaldo.resourceStatus.service;

import com.morais.reginaldo.resourceStatus.properties.AppManifestConfigurationProperties;
import com.morais.reginaldo.resourceStatus.representation.ResourceStatusResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

@Slf4j
@AllArgsConstructor
@EnableConfigurationProperties(AppManifestConfigurationProperties.class)
@Service
public class ResourceStatusServiceImpl implements ResourceStatusService {

    private AppManifestConfigurationProperties appManifestConfigurationProperties;

    @Override
    public ResourceStatusResponse get() {

        return new ResourceStatusResponse(
                appManifestConfigurationProperties.getCreatedBy(),
                appManifestConfigurationProperties.getBuildJDK(),
                appManifestConfigurationProperties.getApplicationName(),
                appManifestConfigurationProperties.getImplementationBuild(),
                appManifestConfigurationProperties.getImplementationVersion());
    }
}
