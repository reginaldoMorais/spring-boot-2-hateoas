package com.morais.reginaldo.user.enumeration;

import com.morais.reginaldo.user.exception.GitNotFoundException;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum Git {

    GITHUB,
    GITLAB,
    ATLASIAN_STASH,
    ATLASIAN_BITBUCKET;

    public static Git fromString(final String gitRepository) {

        try {
            return Git.valueOf(gitRepository);
        } catch (final IllegalArgumentException e) {
            throw new GitNotFoundException("Any Git Repository Mapping founded");
        }
    }
}