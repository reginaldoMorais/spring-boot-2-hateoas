package com.morais.reginaldo.configuration.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "rest-template")
public class RestTemplateConfigurationProperties {

    private int connectTimeout;

    private int connectionRequestTimeout;

    private int socketTimeout;
}
