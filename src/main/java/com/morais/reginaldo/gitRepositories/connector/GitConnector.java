package com.morais.reginaldo.gitRepositories.connector;

import com.morais.reginaldo.gitRepositories.representation.GitResponse;

public interface GitConnector<T extends GitResponse> {

    T get(String user);
}
