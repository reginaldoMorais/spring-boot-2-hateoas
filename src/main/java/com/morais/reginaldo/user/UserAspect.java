package com.morais.reginaldo.user;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class UserAspect {

    @Before("execution(* com.morais.reginaldo.user.service.UserService.*(..))")
    public void before(final JoinPoint joinPoint) { }

    @After("execution(* com.morais.reginaldo.user.service.UserService.*(..))")
    public void after(final JoinPoint joinPoint) { }
}
