package com.morais.reginaldo.configuration.properties;

import lombok.Data;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "mongodb")
public class MongoDBConfigurationProperties {

    private MongoProperties props = new MongoProperties();
}
