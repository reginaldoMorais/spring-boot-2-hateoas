package com.morais.reginaldo.user.exception;

import com.morais.reginaldo.user.representation.ErrorMessageResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.stream.Collectors;

@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class UserExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(HttpServerErrorException.class)
    protected ResponseEntity<ErrorMessageResponse> handleServerError(final HttpServerErrorException e) {

        ErrorMessageResponse message = new ErrorMessageResponse(e.getMessage());
        return new ResponseEntity<ErrorMessageResponse>(message, e.getStatusCode());
    }

    @ExceptionHandler(HttpClientErrorException.class)
    protected ResponseEntity<ErrorMessageResponse> handleClientError(final HttpClientErrorException e) {

        ErrorMessageResponse message = new ErrorMessageResponse(e.getMessage());
        return new ResponseEntity<ErrorMessageResponse>(message, e.getStatusCode());
    }

    @ExceptionHandler(UserNotFoundException.class)
    protected ResponseEntity<ErrorMessageResponse> handleUserNotFoundException(final UserNotFoundException e) {

        ErrorMessageResponse message = new ErrorMessageResponse(e.getMessage());
        return new ResponseEntity<ErrorMessageResponse>(message, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UserStatusNotFoundException.class)
    protected ResponseEntity<ErrorMessageResponse> handleUserStatusNotFoundException(
            final UserStatusNotFoundException e) {

        ErrorMessageResponse message = new ErrorMessageResponse(e.getMessage());
        return new ResponseEntity<ErrorMessageResponse>(message, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException e,
                                                                  final HttpHeaders headers,
                                                                  final HttpStatus status,
                                                                  final WebRequest request) {

        String message = "";

        if (hasBindingErrors(e)) {
            message = e.getBindingResult()
                    .getAllErrors().stream()
                    .map(this::formatBindingErrorMessage)
                    .collect(Collectors.joining("; "));
        }

        ErrorMessageResponse response = new ErrorMessageResponse(message);
        return ResponseEntity.badRequest().body(response);
    }

    private String formatBindingErrorMessage(final ObjectError error) {

        if (error instanceof FieldError) {
            FieldError fieldError = (FieldError) error;
            return String.format("'%s: %s'", fieldError.getField(), fieldError.getDefaultMessage());
        }

        return error.getDefaultMessage();
    }

    private boolean hasBindingErrors(final MethodArgumentNotValidException e) {

        return e != null && e.getBindingResult() != null && e.getBindingResult().getAllErrors() != null;
    }
}
