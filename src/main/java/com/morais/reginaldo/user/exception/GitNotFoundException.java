package com.morais.reginaldo.user.exception;

public class GitNotFoundException extends RuntimeException {

    public GitNotFoundException() { }

    public GitNotFoundException(final String message) {
        super(message);
    }

    public GitNotFoundException(final String message, final Throwable throwable) {
        super(message, throwable);
    }
}
